## Requirements

- Node 12.17.0+
- PostgreSQL 12.3+


## Install / Run

1. Install dependencies:

```
npm i

```
2. Update configs / set environment variables (see section below).
```
```
3. Apply DB migrations 

```
npm run migrations_latest
```

4. Run the app

```
npm run dev
```

### Environment variables

-`PORT` -NodeJS Listen Port
-`DB_HOST`
-`DB_NAME`
-`DB_PORT`
-`DB_USER`
-`DB_PASS`-Your Database  Configuration
-`AUTHORIZATION_USER`-Basic authorization user ('test' with sha256 by default)
-`AUTHORIZATION_PASS`-Basic authorization password ('test' with sha256 by default)
-`ACCESS_TOKEN_SECRT`-Random kay for JWT usage (random text with sha256 by default)

#### Routing 

- ##### Routing (`./routs`)
    ##### Main Express Rout  (`./routs/index.js`)
    ##### Branch of Rout  (`./routs/routs.js`)


#### Layers of the application

- ##### Controller layer (`./controller`)
- ##### Service layer (`./services`)
- ##### Database layer (`./db`)