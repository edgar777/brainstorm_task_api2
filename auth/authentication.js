require('dotenv').config();
const jwt=require('jsonwebtoken');

const authentication=(req,resp,next)=>{
    let token=req.headers['authorization'];
    if(token===null) return res.sendStatus(403);
    jwt.verify(token, process.env.ACCESS_TOKEN_SECRT, async(err)=>{
        if(err)return resp.status(401).json({ message: 'Missing Authentication Header'});
    })
    next();
}

module.exports=authentication;

