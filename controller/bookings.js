const  bookingService=require('../services/bookings');

class Booking{
    async create(req,resp){
        try{
            const data=await bookingService.create(req.body);
            resp.status(201).json(data);
        }
        catch(err){
            resp.status(400).json(err.constraint);
        }
    }

    async update(req,resp){
        try{
            const data=await bookingService.update(req.body,req.params.id);
            resp.status(200).json(data);
        }
        catch(err){
            resp.status(400).json(err);
        }
    }

}

module.exports=new Booking();