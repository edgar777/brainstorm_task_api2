const  mealService=require('../services/meals');

class Meals{
    async create(req,resp){
        try{
            const data=await mealService.create(req.body);
            resp.status(201).json(data);
        }
        catch(err){
            resp.status(400).json(err.constraint);
        }
    }

    async getAll(req,resp){
        try{
            const data=await mealService.getAll();
            resp.status(200).json(data);
        }
        catch(err){
            console.log(err)
            resp.status(400).json(err);
        }
    }
}

module.exports=new Meals();