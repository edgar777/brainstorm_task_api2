const  userService=require('../services/users');

class Users{
    async create(req,resp){
        try{
            const data=await userService.create(req.body);
            resp.status(201).json(data);
        }
        catch(err){
            resp.status(400).json(err.constraint);
        }
    }

    async login(req,resp){
        try{
            const data=await userService.login(req.body);
            resp.status(200).json(data);
        }
        catch(err){
            console.log(err)
            resp.status(400).json(err);
        }
    }
}

module.exports=new Users();