
exports.up = async (knex)=>{
  await knex.schema.createTable('users',(table)=>{
    table.increments('id');
    table.string('name').notNullable();
    table.string('lname').notNullable();
    table.string('email').notNullable().unique();
    table.string('pass').notNullable();
    table.string('created_at').notNullable();
    table.string('updated_at').notNullable();
   })
};

exports.down = async(knex) =>{
  await knex.schema.dropTable('users');
};
