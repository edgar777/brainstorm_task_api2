
exports.up = async (knex)=>{
    await knex.schema.createTable('bookings',(table)=>{
      table.increments('id');
      table.integer('persons').notNullable();
      table.string('purpose');
      table.string('date_time').notNullable().unique();
      table.string('comment').notNullable();
      table.string('created_at').notNullable();
      table.string('updated_at').notNullable();
     })
};
  
exports.down = async(knex) =>{
  await knex.schema.dropTable('bookings');
};
  