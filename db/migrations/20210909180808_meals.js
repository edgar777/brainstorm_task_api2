
exports.up = async (knex)=>{
    await knex.schema.createTable('meals',(table)=>{
      table.increments('id');
      table.string('title').notNullable();
      table.string('price').notNullable(),
      table.string('recipe');
      table.string('portion');
      table.string('created_at').notNullable();
      table.string('updated_at').notNullable();
     })
};
  

exports.down = async(knex) =>{
    await knex.schema.dropTable('meals');
};
  