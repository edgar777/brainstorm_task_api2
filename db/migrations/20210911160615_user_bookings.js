
exports.up = async (knex)=>{
    await knex.schema.createTable('user_bookings',(table)=>{
        table.increments('id');
        table.integer('user_id').notNullable();
        table.integer('booking_id').notNullable(),
        table.string('created_at').notNullable();
        table.string('updated_at').notNullable();
    })
};

exports.down = async(knex) =>{
    await knex.schema.dropTable('user_bookings');
};
