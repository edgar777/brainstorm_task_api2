
exports.up = async (knex)=>{
    await knex.schema.createTable('booking_meals',(table)=>{
        table.increments('id');
        table.integer('booking_id').notNullable(),
        table.integer('meal_id').notNullable();
        table.string('created_at').notNullable();
        table.string('updated_at').notNullable();
    })
};

exports.down = async(knex) =>{
    await knex.schema.dropTable('booking_meals');
};
