const MainModel=require('./main_model');

class BookingDAO extends MainModel{

    async create(dao,daoMain){
        const bookings=await super.create('bookings',dao);
        const daoNew={
            user_id:daoMain.user_id,
            booking_id:bookings[0].id,
        }
        await super.create('user_bookings',daoNew);
        return bookings[0];
    }

    async update(dao,daoMeal,id){
        const bookings=await super.update('bookings',dao,id);
        for(let i=0;i<daoMeal.length;i++){
            await super.create('booking_meals',daoMeal[i]);
        }
        return {update:bookings};
    }

}


module.exports=new BookingDAO();
