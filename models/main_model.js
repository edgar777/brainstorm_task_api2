const db=require('../db/db');

class MainModel{
    async create(tbName,dao){
        dao.created_at=new Date().getTime();
        dao.updated_at=new Date().getTime();
        return await db(tbName).insert(dao).returning('*');
    }

    async get(tbName,field,where){
        return await db(tbName).select(field).where(where);
    }

    async get(tbName){
        return await db(tbName).select('*')
    }

    async update(tbName,dao,id){
        dao.updated_at=new Date().getTime();
        return await db(tbName).update(dao).where('id',id);
    }

}

module.exports=MainModel;