const MainModel=require('./main_model');

class MealDAO extends MainModel{

    async create(dao){
        return await super.create('meals',dao);
    }
    
    async getAll(){
        return await super.get('meals')
    }
    
}


module.exports=new MealDAO();
