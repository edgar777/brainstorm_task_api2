const MainModel=require('./main_model');
const crypto = require('crypto');
const jwt=require('jsonwebtoken');

class UserDAO extends MainModel{
    generatePasswordHash(password) {
       const hash = crypto.createHash('sha256');
       const hashPass=hash.update(password).digest('hex');
       return hashPass;
    }

    checkPassword(sendPass,pass){
        if(this.generatePasswordHash(sendPass)===pass)
            return true;
        else
            return false;
    }

    getJWT(user){
       return jwt.sign(user, process.env.ACCESS_TOKEN_SECRT, {expiresIn:'12h'});
    }

    async create(dao){
        dao.pass=this.generatePasswordHash(dao.pass);
        return await super.create('users',dao);
    }
    
    async login(dao){
        const data=await super.get('users',['pass'],{email:dao.email});
        const login=this.checkPassword(dao.pass,data[0].pass);
        const accessToken=login?this.getJWT(dao):'';
        return {login,accessToken};
    }
    
}


module.exports=new UserDAO();
