const express=require('express');
const router=express.Router();
const RouterMap=require('../routs/routs');
const RouterMapApi=RouterMap.api;
const RouterMapAdmin=RouterMap.admin;

for (let i=0; i<RouterMapApi.length; i++) {
    router[RouterMapApi[i].method](
        `/api/${RouterMapApi[i].endpoint}/${RouterMapApi[i].params}`,
        RouterMapApi[i].mdw,
        RouterMapApi[i].callcack,
    )
}

for (let i=0; i<RouterMapAdmin.length; i++) {
    router[RouterMapAdmin[i].method](
        `/admin/${RouterMapAdmin[i].endpoint}/${RouterMapAdmin[i].params}`,
        RouterMapAdmin[i].mdw,
        RouterMapAdmin[i].callcack,
    )
}


module.exports=router;

