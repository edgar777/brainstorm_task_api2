const authorization=require('../auth/authorization');
const authentication=require('../auth/authentication');

const User=require('../controller/users');
const Meals=require('../controller/meals');
const Booking=require('../controller/bookings');

const RouterMap={
    api:[
        {
            method:'post',
            endpoint:'user',
            params:'',
            mdw:authorization,
            callcack:User.create,
        },
        {
            method:'post',
            endpoint:'user/login',
            params:'',
            mdw:authorization,
            callcack:User.login,
        },
        {
            method:'post',
            endpoint:'meal',
            params:'',
            mdw:authorization,
            callcack:Meals.create,
        },
        {
            method:'get',
            endpoint:'meal',
            params:'',
            mdw:authorization,
            callcack:Meals.getAll,
        },
        {
            method:'post',
            endpoint:'booking',
            params:'',
            mdw:authentication,
            callcack: Booking.create,
        },
        {
            method:'patch',
            endpoint:'booking',
            params:':id',
            mdw:authentication,
            callcack: Booking.update,
        }
    ],
    admin:[

    ]
}

module.exports=RouterMap;