const BookingDAO=require('../models/bookings');

class MealsService{
    copyObj(object){
        const newObj={};
        for (const property in object) {
            newObj[property]=object[property];
        }
        return newObj;
    }

    crtDaoRelation(meals,id){
        const newDao=[];
        for(let i=0; i<meals.length; i++){
            newDao[i]={
                booking_id:id,
                meal_id:meals[i]
            }
        }
        return newDao;
    }

    async create(dao){
        const daoMain=this.copyObj(dao);
        delete dao.user_id;
        return await BookingDAO.create(dao,daoMain);
    }
    
    async update(dao,id){
        const daoMeal=this.crtDaoRelation(dao.meals,id);
        delete dao.meals;
        return await BookingDAO.update(dao,daoMeal,id);
    }

}

module.exports=new MealsService;