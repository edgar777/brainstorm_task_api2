const MealDAO=require('../models/meals');

class MealsService{
    async create(dao){
        return await MealDAO.create(dao);
    }
    async getAll(){
        return await MealDAO.getAll();
    }
}

module.exports=new MealsService;