const UserDAO=require('../models/users');

class UsersService{
    async create(dao){
        return await UserDAO.create(dao);
    }
    
    async login(dao){
        return await UserDAO.login(dao);
    }
}

module.exports=new UsersService;